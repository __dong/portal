portalModule.controller('verkaufenController', function ($scope, $http) {

//Seller
    $scope.seller={};
    $scope.sellers={};
    $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.loadSellers = function() {
                $http({method: "GET", url: "http://localhost:8080/allSellers"}).
                            then(function(response) {
                                // success callback
                                $scope.sellers = response.data;

                                console.log($scope.sellers);
                                console.log($scope.status);
                            }, function(response) {
                                // failure call back
                                $scope.sellers = response.data || 'Request failed';
                                $scope.status = response.status;

                                console.log($scope.data);
                                console.log($scope.status);
                          });
              };

    $scope.loadSellers();

    $scope.saveSeller = function(seller) {
            console.log("--> add new seller");
            console.log($scope.seller);

            $http.post("http://localhost:8080/addSeller", $scope.seller)
               .then(
                   function(response){
                     // success callback
                     console.log("juhu"+ response.data);

                     $scope.loadSellers();
                   },
                   function(response){
                     // failure callback
                     console.log("fehler");
                   }
                );
             };

//Car
    $scope.car={};
    $scope.cars={};
    $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.loadCars = function() {
                $http({method: "GET", url: "http://localhost:8080/allCars"}).
                            then(function(response) {
                                // success callback
                                $scope.cars = response.data;

                                console.log($scope.cars);
                                console.log($scope.status);
                            }, function(response) {
                                // failure call back
                                $scope.cars = response.data || 'Request failed';
                                $scope.status = response.status;

                                console.log($scope.data);
                                console.log($scope.status);
                          });
              };

    $scope.loadCars();

    $scope.car={};

    $scope.saveCar = function() {
            console.log("--> add new car");
            console.log($scope.car);

            $http.post("http://localhost:8080/addCar", $scope.car)
               .then(
                   function(response){
                     // success callback
                     console.log("Car wurde hinzugefügt: "+ response.data);

                     $scope.loadCars();
                   },
                   function(response){
                     // failure callback
                     console.log("Fehler beim Hinzufügen eines Cars " + response.status + " " + $scope.car + " data: " + response.data);
                   }
                );
             };

$scope.infoo=function(){
    console.log($scope.car)};

    $scope.addCar2 = function(car) {
            console.log("--> add new CAR2");
            console.log($scope.car);

            $http.post("http://localhost:8080/demo/addCar2", $scope.car)
               .then(
                   function(response){
                     // success callback
                     console.log(response.data);
                     console.log("juhu"+ response.data);


                     $scope.loadCars();
                   },
                   function(response){
                     // failure callback
                                          console.log("fehler " + response.status + " " + $scope.car + " data: " + response.data);

                   }
                );
             };


    $scope.addCar3 = function(car) {
            console.log("--> add new CAR3");
            console.log($scope.car);

            $http.post("http://localhost:8080/demo/addCar3", $scope.car)
               .then(
                   function(response){
                     // success callback
                     console.log(response.data);
                     console.log("juhu"+ response.data);


                     $scope.loadCars();
                   },
                   function(response){
                     // failure callback
                                          console.log("fehler " + response.status + " " + $scope.car + " data: " + response.data);

                   }
                );
             };
//Locations
    $scope.location={};
    $scope.locations={};
    $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.loadLocations = function() {
                $http({method: "GET", url: "http://localhost:8080/allLocations"}).
                            then(function(response) {
                                // success callback
                                $scope.locations = response.data;

                                console.log($scope.locations);
                                console.log($scope.status);
                            }, function(response) {
                                // failure call back
                                $scope.locations = response.data || 'Request failed';
                                $scope.status = response.status;

                                console.log($scope.data);
                                console.log($scope.status);
                          });
              };
    $scope.loadLocations();

    $scope.saveLocation = function(location) {
            console.log("--> add new location");
            console.log($scope.location);

            $http.post("http://localhost:8080/addLocation", $scope.location)
               .then(
                   function(response){
                     // success callback
                     console.log(response.data);

                     $scope.loadLocations();
                   },
                   function(response){
                     // failure callback
                   }
                );
             };


//Brands
    $scope.brand={};
    $scope.brands={};
    $http.defaults.headers.post["Content-Type"] = "application/json";
        $scope.loadBrands = function() {
                $http({method: "GET", url: "http://localhost:8080/allBrands"}).
                            then(function(response) {
                                // success callback
                                $scope.brands = response.data;

                                console.log($scope.brands);
                                console.log($scope.status);
                            }, function(response) {
                                // failure call back
                                $scope.brands = response.data || 'Request failed';
                                $scope.status = response.status;

                                console.log($scope.data);
                                console.log($scope.status);
                          });
              };
    $scope.loadBrands();

    $scope.saveBrand = function(brand) {
            console.log("--> add new brand");
            console.log($scope.brand);

            $http.post("http://localhost:8080/addBrand", $scope.brand)
               .then(
                   function(response){
                     // success callback
                     console.log(response.data);

                     $scope.loadBrands();
                   },
                   function(response){
                     // failure callback
                   }
                );
             };


    $scope.addBrand2 = function(brand) {
            console.log("--> add new brand2");
            console.log($scope.brand);

            $http.post("http://localhost:8080/demo/addBrand2", $scope.brand)
               .then(
                   function(response){
                     // success callback
                     console.log(response.data);

                     $scope.loadBrands();
                   },
                   function(response){
                     // failure callback
                   }
                );
             };

    $scope.selected = undefined;

//Save All

    $scope.saveAll = function () {
        $scope.saveSeller();
        $scope.saveBrand();
        $scope.saveLocation();
        $scope.saveCar();

    };

//Search
    $scope.search = {};
    $scope.carInput = {};

    $scope.applySearch = function() {
        for(prop in $scope.userInput) {
            $scope.search[prop] = $scope.carInput[prop];
        }
    };
//Search Price
$scope.priceFilter = function (car) {
    var price = parseFloat(car.price);
    var min = parseFloat($scope.minPrice);
    var max = parseFloat($scope.maxPrice);

    if (!price) {
      return false;
    }

    if(min && price < min) {
      return false;
    }

    if(max && price > max) {
      return false;
    }

    return true;
  };
//Search horsepower
$scope.horsepowerFilter = function (car) {
    var horsepower = parseInt(car.horsepower);
    var min = parseInt($scope.minHorsepower);
    var max = parseInt($scope.maxHorsepower);

    if (!horsepower) {
      return false;
    }

    if(min && horsepower < min) {
      return false;
    }

    if(max && horsepower > max) {
      return false;
    }

    return true;
  };

//Search KM
$scope.kmFilter = function (car) {
    var km = parseInt(car.km);
    var max = parseInt($scope.maxKm);

    if (!km){
    return false;
    }
    if(max && km > max){
    return false
    }
    return true;
};

//Search EZ
$scope.firstRegistrationFilter = function (car) {
    var registration = parseInt(car.firstRegistration);
    var min = parseInt($scope.minFirstRegistration);
    var max = parseInt($scope.maxFirstRegistration);

    if (!firstRegistration) {
      return false;
    }

    if(min && firstRegistration < min) {
      return false;
    }

    if(max && firstRegistration > max) {
      return false;
    }

    return true;
  };


});


