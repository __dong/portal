portalModule.controller('kaufenController',['$scope','$http','$filter', function ($scope, $http, ModalService ) {
    $scope.car={};
    $scope.cars=[];
    $http.defaults.headers.post["Content-Type"] = "application/json";
    $scope.loadCars = function() {
        $http({method: "GET", url: "http://localhost:8080/allCars"}).
        then(function(response) {
            // success callback
            $scope.cars = response.data;
            console.log($scope.cars);
        }, function(response) {
            // failure call back
            $scope.cars = response.data || 'Request failed';
            $scope.status = response.status;

            console.log($scope.data);
            console.log($scope.status);
        });
    };

    $scope.loadCars();
//table
    $scope.isLoading = false;
    $scope.columns=['ID','Marke/Modell','Kraftstoff','Leistung (PS)', 'Erstzulassung','Kilometerstand','Preis','PLZ/Ort','More']

    $scope.bla=function(){
        alert('test');
    };

    $scope.deleteCar = function (id) {
        //var carId = $scope.cars[.id;
        $http({method: "DELETE", url:"http://localhost:8080/cars/" + id})
            .then(function (response) {
                console.log("Auto wurde gelöscht");
                $scope.loadCars();
                
            },function (response) {
                console.log('Fehler beim Löschen');
                
                }
            );
    };
    /*
     $scope.DeleteUser = function (id) {
     var userId = $scope.users[id].Id;
     $http.delete('/api/users/' + userId)
     .success(function (data) {
     $scope.error = "error " + data;
     });
     }
     */

//Search Price
    $scope.priceFilter = function (car) {
        var price = parseFloat(car.price);
        var min = parseFloat($scope.minPrice);
        var max = parseFloat($scope.maxPrice);

        if (!price) {
            return false;
        }

        if(min && price < min) {
            return false;
        }

        if(max && price > max) {
            return false;
        }

        return true;
    };
//Search horsepower
    $scope.horsepowerFilter = function (car) {
        var horsepower = parseInt(car.horsepower);
        var min = parseInt($scope.minHorsepower);
        var max = parseInt($scope.maxHorsepower);

        if (!horsepower) {
            return false;
        }

        if(min && horsepower < min) {
            return false;
        }

        if(max && horsepower > max) {
            return false;
        }

        return true;
    };

//Search KM
        $scope.kmFilter = function (car) {
            var km = parseInt(car.km);
            var min = parseInt($scope.minKm);
            var max = parseInt($scope.maxKm);

            if (!km) {
                return false;
            }

            if(min && km < min) {
                return false;
            }

            if(max && km > max) {
                return false;
            }

            return true;
        };
//Search EZ
    $scope.minFirstRegistration={};
    $scope.maxFirstRegistration={};

    $scope.firstRegistrationFilter = function (car) {
        var registration = parseInt(car.firstRegistration);
        var min = parseInt($scope.minFirstRegistration);
        var max = parseInt($scope.maxFirstRegistration);

        if (!firstRegistration) {
            return false;
        }

        if(min && firstRegistration < min) {
            return false;
        }

        if(max && firstRegistration > max) {
            return false;
        }

        return true;
    };

    $scope.show = function() {
        ModalService.showModal({
            templateUrl: 'modal.html',
            controller: "ModalController"
        }).then(function(modal) {
            modal.element.modal();
            modal.close.then(function(result) {
                $scope.message = "You said " + result;
            });
        });
    };
    $scope.close = function(result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };
}]);

