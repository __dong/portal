package com.portal.web;

import com.portal.PortalApplication;
import com.portal.domain.Car;
import com.portal.service.CarService;
import com.portal.web.dto.CarDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created by D064689 on 05.03.2017.
 */
@Controller
public class CarRessource {
    private static final Logger logger = LoggerFactory.getLogger(PortalApplication.class);

    @Autowired
    private CarService carService;

    /**
     *
     * @return all cars
     */
    @RequestMapping(value = "/allCars", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ArrayList<Car>> getAllCars() {
        ArrayList<Car> cars = carService.getAllCars();

        if(cars.isEmpty()) {
            return new ResponseEntity<ArrayList<Car>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ArrayList<Car>>(cars, HttpStatus.OK);
    }

    /**
     *
     * @param input
     * @return
     */
    @RequestMapping(value = "/addCar", method = RequestMethod.POST)
    public ResponseEntity<Car> update(@RequestBody CarDTO input) {
        Car car1 = carService.insertCar(input);
        return new ResponseEntity<Car>(car1, HttpStatus.OK);
    }
}
