package com.portal.web;

import com.portal.domain.Location;
import com.portal.service.LocationService;
import com.portal.web.dto.LocationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created by tdhoang on 24.02.17.
 */
@Controller
public class LocationRessource {

    @Autowired
    LocationService locationService;


    /**
     *
     * @return all locations in db
     */
    @RequestMapping(value = "/allLocations", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ArrayList<Location>> getAllLocations() {
        ArrayList<Location> locations = locationService.getAllLocations();

        if(locations.isEmpty()) {
            return new ResponseEntity<ArrayList<Location>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ArrayList<Location>>(locations, HttpStatus.OK);
    }

    /**
     *
     * @param input
     * @return
     */
    @RequestMapping(value = "/addLocation", method = RequestMethod.POST)
    public ResponseEntity<Location> update(@RequestBody LocationDTO input) {
        Location location1 = locationService.insertLocation(input);
        return new ResponseEntity<Location>(location1, HttpStatus.OK);
    }
}
