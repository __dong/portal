package com.portal.web;

import com.portal.domain.Brand;
import com.portal.service.BrandService;
import com.portal.web.dto.BrandDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

/**
 * Created by tdhoang on 24.02.17.
 */
@Controller
public class BrandRessource {

    @Autowired
    private BrandService brandService;

    /**
     *
     * @return all brands
     */
    @RequestMapping(value = "/allBrands", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ArrayList<Brand>> getAllBrands() {
        ArrayList<Brand> brands = brandService.getAllBrands();

        if(brands.isEmpty()) {
            return new ResponseEntity<ArrayList<Brand>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ArrayList<Brand>>(brands, HttpStatus.OK);
    }

    /**
     *
     * @param input
     * @return
     */
    @RequestMapping(value = "/addBrand", method = RequestMethod.POST)
    public ResponseEntity<Brand> update(@RequestBody BrandDTO input) {
        Brand brand1 = brandService.insertBrand(input);
        return new ResponseEntity<Brand>(brand1, HttpStatus.OK);
    }
}
