package com.portal.web.dto;
import java.util.Date;
/**
 * Created by D064689 on 02.02.2017.
 */
//input object
public class CarDTO {


    private String carType ;  //type ist vorbelastet, bitte ändern
    private Date firstRegistration;
    private int km;
    private Float price;
    private int horsepower;
    private String fuelType;
    private String gear;
    private String color;
    private String badge;
    private String locationName;
    private String locationState;
    private int locationPostalCode;
    private String brandName;
    private String brandModel;
    private String sellerFirstName;
    private String sellerLastName;
    private String sellerCompanyName;
    private String sellerEmail;
    private int sellerMobileNumber;
    private int sellerTelephoneNumber;



    public CarDTO() {
    }


    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public Date getFirstRegistration() {
        return firstRegistration;
    }

    public void setFirstRegistration(Date firstRegistration) {
        this.firstRegistration = firstRegistration;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getGear() {
        return gear;
    }

    public void setGear(String gear) {
        this.gear = gear;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public Integer getLocationPostalCode() {
        return locationPostalCode;
    }

    public void setLocationPostalCode(Integer locationPostalCode) {
        this.locationPostalCode = locationPostalCode;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandModel() {
        return brandModel;
    }

    public void setBrandModel(String brandModel) {
        this.brandModel = brandModel;
    }

    public String getSellerFirstName() {
        return sellerFirstName;
    }

    public void setSellerFirstName(String sellerFirstName) {
        this.sellerFirstName = sellerFirstName;
    }

    public String getSellerLastName() {
        return sellerLastName;
    }

    public void setSellerLastName(String sellerLastName) {
        this.sellerLastName = sellerLastName;
    }

    public String getSellerCompanyName() {
        return sellerCompanyName;
    }

    public void setSellerCompanyName(String sellerCompanyName) {
        this.sellerCompanyName = sellerCompanyName;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public Integer getSellerMobileNumber() {
        return sellerMobileNumber;
    }

    public void setSellerMobileNumber(Integer sellerMobileNumber) {
        this.sellerMobileNumber = sellerMobileNumber;
    }

    public Integer getSellerTelephoneNumber() {
        return sellerTelephoneNumber;
    }

    public void setSellerTelephoneNumber(Integer sellerTelephoneNumber) {
        this.sellerTelephoneNumber = sellerTelephoneNumber;
    }
}
