package com.portal.web.dto;

import com.portal.domain.Car;
import com.portal.domain.Seller;

import java.util.Set;

/**
 * Created by D064689 on 02.02.2017.
 */
public class LocationDTO {

    private int id;

    private String locationName;
    private int postalCode;
    private String state;
    private Set<Car> cars;
    private Set<Seller> sellers;

    public void setId(int id) {
        this.id = id;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

    public void setSellers(Set<Seller> sellers) {
        this.sellers = sellers;
    }

    public int getId() {
        return id;
    }

    public String getLocationName() {
        return locationName;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public String getState() {
        return state;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public Set<Seller> getSellers() {
        return sellers;
    }
}
