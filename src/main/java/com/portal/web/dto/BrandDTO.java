package com.portal.web.dto;

/**
 * Created by D064689 on 02.02.2017.
 */

public class BrandDTO {

    private String brandName;
    private String model;
    private int year;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
