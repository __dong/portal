package com.portal.web;

import com.portal.domain.Seller;
import com.portal.service.SellerService;
import com.portal.web.dto.SellerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Created by tdhoang on 24.02.17.
 */
@Controller
public class SellerResource {

    @Autowired
    SellerService sellerService;

    /**
     *
     * @return
     */
    @RequestMapping(value = "/allSellers", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ArrayList<Seller>> getAllSeller() {
        ArrayList<Seller> sellers = sellerService.getAllSellers();

        if(sellers.isEmpty()) {
            return new ResponseEntity<ArrayList<Seller>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ArrayList<Seller>>(sellers, HttpStatus.OK);
    }

    /**
     *
     * @param input
     * @return
     */
    @RequestMapping(value = "/addSeller", method = RequestMethod.POST)
    public ResponseEntity<Seller> update(@RequestBody SellerDTO input) {
        Seller seller1 = sellerService.insertSeller(input);
        return new ResponseEntity<Seller>(seller1, HttpStatus.OK);
    }



}

