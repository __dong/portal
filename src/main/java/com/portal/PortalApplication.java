package com.portal;

import com.portal.domain.Location;
import com.portal.domain.Seller;
import com.portal.domain.Car;
import com.portal.repository.BrandRepository;
import com.portal.repository.CarRepository;
import com.portal.repository.LocationRepository;
import com.portal.repository.SellerRepository;
import com.portal.sqlScriptRunner.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Date;

@SpringBootApplication
public class PortalApplication implements CommandLineRunner{
	private static final Logger logger = LoggerFactory.getLogger(PortalApplication.class);

	@Autowired
	private SellerRepository sellerRepository;

	@Autowired
	private CarRepository carRepository;

	@Autowired
	private BrandRepository brandRepository;

    @Autowired
    private LocationRepository locationRepository;

	public static void main(String[] args) {
		SpringApplication.run(PortalApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {



        /*
        Insert brands to db !
         */

        // Create MySql Connection
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3305/portaldb", "dev", "dev");
        Statement stmt = null;
        try {
            // Initialize object for ScripRunner
            ScriptRunner sr = new ScriptRunner(con, false, false);

            // Give the input file to Reader
            Resource resource = new ClassPathResource("data-brand.sql");
            FileReader reader = new FileReader(resource.getFile());
            logger.info("HIER: " + resource + reader);

            Resource resource2 = new ClassPathResource("data-location.sql");
            FileReader reader2 = new FileReader(resource2.getFile());

            // Exctute script
            // ACTIVATE INSERT HERE // !!!

            sr.runScript(reader); //insert brands
            sr.runScript(reader2); //insert locations

        } catch (Exception e) {
            System.err.println("Failed to Execute FileReader/Resource"
                    + " The error is " + e.getMessage());
        }

		for (Seller seller: sellerRepository.findAll()) {
			logger.info("LOGGER: " + seller.toString());
		}

		/**
		 * DummyDaten in DB
		 */
		Seller seller1 = new Seller();

		seller1.setEmail("sc@bigbang.de");
		seller1.setCompanyName("Bigbang");
		seller1.setFirstName("Sheldon");
		seller1.setLastName("Cooper");
		seller1.setTelephoneNumber(123456789);
		seller1.setMobileNumber(987654321);
		seller1.setLocation(locationRepository.findOne(1));
		sellerRepository.save(seller1);

		Location location1 = new Location();
		location1.setLocationName("Dresden");
		location1.setPostalCode("01067");
		location1.setState("Sachsen");

		locationRepository.save(location1);

		Seller seller2 = new Seller();

		seller2.setEmail("tp@kdl.de");
		seller2.setCompanyName("Disney");
		seller2.setFirstName("Timon");
		seller2.setLastName("Pumba");
		seller2.setMobileNumber(456789);
		seller2.setTelephoneNumber(98724452);
		seller2.setLocation(locationRepository.findOne(2));
		sellerRepository.save(seller2);

		Location location2 = new Location();
		location2.setLocationName("Königsstadt");
		location2.setPostalCode("01010");
		location2.setState("Jungle");

		locationRepository.save(location2);


		Seller seller3 = new Seller();

		seller3.setEmail("kopa@kdl.de");
		seller3.setCompanyName("Disney");
		seller3.setFirstName("Kopa");
		seller3.setLastName("Lion");
		seller3.setMobileNumber(3456789);
		seller3.setTelephoneNumber(94285829);
		seller3.setLocation(locationRepository.findOne(3));
		sellerRepository.save(seller3);

		Location location3 = new Location();
		location3.setLocationName("Königsstadt");
		location3.setPostalCode("01010");
		location3.setState("Jungle");

		locationRepository.save(location3);

		Location location4 = new Location();
		location4.setLocationName("Hoyerswerda");
		location4.setPostalCode("02977");
		location4.setState("Sachsen");

		locationRepository.save(location4);
		Seller seller4 = new Seller();

		seller4.setEmail("jürgen@laphroaig.de");
		seller4.setCompanyName("Scotch Cars");
		seller4.setFirstName("Jürgen");
		seller4.setLastName("Laphroaig");
		seller4.setTelephoneNumber(1234876);
		seller4.setMobileNumber(987699991);
		seller4.setLocation(locationRepository.findOne(4));
		sellerRepository.save(seller4);



		Car car1 = new Car();
		car1.setCarType("Kombi");
		Date date1 = new Date();
		car1.setFirstRegistration(date1);
		car1.setKm(10000);
		car1.setColor("Schwarz");
		car1.setFuelType("Benzin");
		car1.setBadge("Euro5");
		car1.setGear("Automatik");
		car1.setHorsepower(200);
		car1.setPrice(12000.f);
		car1.setLocation(location1);
		car1.setSeller(seller1);
		car1.setBrand(brandRepository.findOne(1));
		carRepository.save(car1);

		Car car2 = new Car();
		car2.setCarType("Kleinwagen");
		Date date2 = new Date();
		car2.setFirstRegistration(date1);
		car2.setKm(999999);
		car2.setColor("Blau");
		car2.setFuelType("Diesel");
		car2.setBadge("Euro2");
		car2.setGear("Manuell");
		car2.setHorsepower(100);
		car2.setPrice(15000.f);
		car2.setLocation(location2);
		car2.setSeller(seller2);
		car2.setBrand(brandRepository.findOne(2));
		carRepository.save(car2);

		Car car3 = new Car();
		car3.setCarType("Kombi");
		Date date3 = new Date();
		car3.setFirstRegistration(date3);
		car3.setKm(50000);
		car3.setColor("Schwarz");
		car3.setFuelType("Diesel");
		car3.setBadge("Euro5");
		car3.setGear("Manuell");
		car3.setHorsepower(170);
		car3.setPrice(25000.f);
		car3.setLocation(location4);
		car3.setSeller(seller4);
		car3.setBrand(brandRepository.findOne(12));
		carRepository.save(car3);

		Car car4 = new Car();
		car4.setCarType("Coupe");
		Date date4 = new Date();
		car4.setFirstRegistration(date4);
		car4.setKm(110000);
		car4.setColor("Blau");
		car4.setFuelType("Diesel");
		car4.setBadge("Euro2");
		car4.setGear("Manuell");
		car4.setHorsepower(300);
		car4.setPrice(35000.f);
		car4.setLocation(location3);
		car4.setSeller(seller3);
		car4.setBrand(brandRepository.findOne(10));
		carRepository.save(car4);

		//carRepository.delete(1);
		//System.out.print("DELETE CAR1");

	}



}


