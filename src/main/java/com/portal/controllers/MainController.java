package com.portal.controllers;

import com.portal.PortalApplication;
import com.portal.domain.Brand;
import com.portal.domain.Car;
import com.portal.domain.Location;
import com.portal.domain.Seller;
import com.portal.repository.BrandRepository;
import com.portal.repository.CarRepository;
import com.portal.repository.LocationRepository;
import com.portal.repository.SellerRepository;
import com.portal.web.dto.BrandDTO;
import com.portal.web.dto.CarDTO;
import com.portal.web.dto.CarInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tdhoang on 24.02.17.
 * Test REST und TemplateEngine
 */
@Controller
@RequestMapping("/demo")
public class MainController {
    private static final Logger logger = LoggerFactory.getLogger(PortalApplication.class);

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    SellerRepository sellerRepository;


    @GetMapping("/addBrand")
    public @ResponseBody String addBrand(@RequestParam String brandName, @RequestParam String model, @RequestParam Integer year) {
        Brand brand1 = new Brand();
        brand1.setBrandName(brandName);
        brand1.setModel(model);
        brand1.setYear(year);
        brandRepository.save(brand1);
        return "Brand added";
    }

    @RequestMapping(value = "/addBrand2", method = RequestMethod.POST)
    public ResponseEntity<Brand> update(@RequestBody BrandDTO input) {
        Brand brand1 = new Brand();
        brand1.setBrandName(input.getBrandName());
        brand1.setModel(input.getModel());
        brandRepository.save(brand1);
        return new ResponseEntity<Brand>(brand1, HttpStatus.OK);
    }

    @RequestMapping(value = "/addCar2", method = RequestMethod.POST)
    public ResponseEntity<Car> update(@RequestBody CarDTO input) {
        Car car1 = new Car();
        Brand brand = new Brand();
        brand.setBrandName(input.getBrandName());
        brand.setModel(input.getBrandModel());

        Location location = new Location();
        location = locationRepository.findOne(1);

        Seller seller = new Seller();
        seller.setLocation(location);
        seller.setTelephoneNumber(input.getSellerTelephoneNumber());
        seller.setMobileNumber(input.getSellerMobileNumber());
        seller.setFirstName(input.getSellerFirstName());
        seller.setLastName(input.getSellerLastName());
        seller.setEmail(input.getSellerEmail());
        seller.setCompanyName(input.getSellerCompanyName());

        car1.setBrand(brand);
        car1.setLocation(location);
        car1.setSeller(seller);

        car1.setCarType(input.getCarType());
        car1.setBadge(input.getBadge());
        car1.setColor(input.getColor());
        car1.setFuelType(input.getFuelType());
        car1.setGear(input.getGear());
        car1.setHorsepower(input.getHorsepower());
        logger.info("ADD CAR 2:" + car1);
        carRepository.save(car1);
        return new ResponseEntity<Car>(car1, HttpStatus.OK);
    }

    @RequestMapping(value = "/addCar3", method = RequestMethod.POST)
    public ResponseEntity<Car> update(@RequestBody CarInput input) {
        Car car = new Car();
        Brand brand = new Brand();
        brand.setBrandName(input.getBrandName());
        brand.setModel(input.getBrandModel());
        car.setBrand(brand);
        car.setColor(input.getCarColor());
        Seller seller = new Seller();
        seller.setFirstName(input.getSellerFirstName());
        brandRepository.save(brand);
        sellerRepository.save(seller);
        carRepository.save(car);

        return new ResponseEntity<Car>(car, HttpStatus.OK);
    }

    @GetMapping(path = "/allBrands")
    public @ResponseBody Iterable<Brand> getAllBrands(){
        return brandRepository.findAll();
    }




}

