package com.portal.controllers;

import com.portal.PortalApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 *Mapping Webseiten
 */

@Controller
public class IndexController {
    private static final Logger logger = LoggerFactory.getLogger(PortalApplication.class);


    @RequestMapping({"/", "/index"})
    public String index() {
        return "index";

    }

    @RequestMapping("/impressum")
    public String impressum() {
        return "impressum";

    }

    @RequestMapping("/datenschutz")
    public String datenschutz() {

        return "datenschutz";
    }

    @RequestMapping("/kaufen")
    public String kaufen() {

        return "kaufen";
    }

    @RequestMapping("/verkaufen")
    public String verkaufen() {

        return "verkaufen";
    }
    @RequestMapping("/mybrand")
    public String mybrand() {

        return "mybrand";
    }
}


