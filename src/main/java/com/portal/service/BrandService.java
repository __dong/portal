package com.portal.service;
import com.portal.domain.Brand;
import com.portal.web.dto.BrandDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by tdhoang on 05.01.17.
 */

@Transactional
public interface BrandService {

    /**
     *
     * @param input
     * @return
     */
    Brand insertBrand(BrandDTO input);

    /**
     *
     * @param id
     */
    void deleteBrand(int id);

    /**
     *
     * @param id
     * @return
     */
    Brand showBrandById(int id);

    /**
     *
     * @param brandName
     * @return
     */
    Brand searchBrandByName(String brandName);

    /**
     *
     * @return
     */
    ArrayList<Brand> getAllBrands();
}
