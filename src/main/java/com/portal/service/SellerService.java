package com.portal.service;
import com.portal.domain.Seller;
import com.portal.web.dto.SellerDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by tdhoang on 05.01.17.
 */

@Transactional
public interface SellerService {

    /**
     *
     * @param input
     * @return
     */
    Seller insertSeller(SellerDTO input);

    /**
     *
     * @param id
     */
    void deleteSeller(int id);

    /**
     *
     * @param companyName
     * @return
     */
    Seller searchSellerByCompanyName(String companyName);

    /**
     *
     * @param id
     * @return
     */
    Seller showSellerById(int id);

    /**
     *
     * @return
     */
    ArrayList<Seller> getAllSellers();

}
