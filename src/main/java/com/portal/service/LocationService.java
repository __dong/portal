package com.portal.service;

import com.portal.domain.Location;
import com.portal.web.dto.LocationDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by tdhoang on 05.01.17.
 */

@Transactional
public interface LocationService {
    /**
     *
     * @param location
     * @return
     */
    Location insertLocation(LocationDTO location);

    /**
     *
     * @param id
     */
    void deleteLocation(int id);

    /**
     *
     * @param id
     * @return
     */
    Location showLocationById(int id);

    /**
     *
     * @param locationName
     * @return
     */
    Location searchLocationByName (String locationName);
    /**
     *
     * @return
     */
    ArrayList<Location> getAllLocations();
}
