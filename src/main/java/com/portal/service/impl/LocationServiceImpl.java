package com.portal.service.impl;
import com.google.common.collect.Lists;
import com.portal.domain.Location;
import com.portal.repository.LocationRepository;
import com.portal.service.LocationService;
import com.portal.web.dto.LocationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by D064689 on 02.02.2017.
 */
@Service
public class LocationServiceImpl
        implements LocationService{

    private Location location;
    @Autowired
    LocationRepository rep;

    public Location insertLocation(LocationDTO location) {
        this.location = new Location();



       this.location.setId(location.getId());
        this.location.setLocationName(location.getLocationName());
        this.location.setState(location.getState());
        this.location.setCars(location.getCars());
        this.location.setSellers(location.getSellers());

        rep.save(this.location);
        return null;
    }

    @Override
    public void deleteLocation(int id) {
        rep.delete(id);
    }

    @Override
    public Location searchLocationByName (String locationName) {
        return rep.findLocationByLocationName(locationName);
    }

    @Override
    public ArrayList<Location> getAllLocations() {
        Iterator<Location> iteratorToArray =rep.findAll().iterator();
        return Lists.newArrayList(iteratorToArray);

    }


    @Override
    public Location showLocationById(int id){
        Location l = new Location();
        l.setId(rep.findOne(1).getId());
        l.setLocationName(rep.findOne(1).getLocationName());
        l.setState(rep.findOne(1).getState());
        l.setCars(rep.findOne(1).getCars());
        l.setSellers(rep.findOne(1).getSellers());

        return l;
    }
}
