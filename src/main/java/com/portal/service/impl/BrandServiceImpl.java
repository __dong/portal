package com.portal.service.impl;

import com.google.common.collect.Lists;
import com.portal.domain.Brand;
import com.portal.repository.BrandRepository;
import com.portal.service.BrandService;
import com.portal.web.dto.BrandDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by D064689 on 02.02.2017.
 */
@Service
public class BrandServiceImpl
        implements BrandService {


    @Autowired
    BrandRepository rep;

    @Override
    public Brand insertBrand(BrandDTO input) {
        Brand brand1 = new Brand();
        brand1.setBrandName(input.getBrandName());
        brand1.setModel(input.getModel());
        brand1.setYear(input.getYear());

        rep.save(brand1);

        return brand1;
    }

    @Override
    public void deleteBrand(int id) {
        rep.delete(id);
    }


    @Override
    public Brand searchBrandByName (String brandName) {
        return rep.findBrandByBrandName(brandName);
    }

    @Override
    public Brand showBrandById(int id) {
        Brand b = new Brand();
        b.setId(rep.findOne(1).getId());
        b.setBrandName(rep.findOne(1).getBrandName());
        b.setModel(rep.findOne(1).getModel());
        b.setYear(rep.findOne(1).getYear());

        return b;
    }

    @Override
    public ArrayList<Brand> getAllBrands() {
        Iterator<Brand> iteratorToArray =rep.findAll().iterator();
        return Lists.newArrayList(iteratorToArray);

    }
}