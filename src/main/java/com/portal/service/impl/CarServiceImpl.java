package com.portal.service.impl;

import com.google.common.collect.Lists;
import com.portal.domain.Brand;
import com.portal.domain.Car;
import com.portal.domain.Location;
import com.portal.domain.Seller;
import com.portal.repository.BrandRepository;
import com.portal.repository.CarRepository;
import com.portal.repository.LocationRepository;
import com.portal.repository.SellerRepository;
import com.portal.service.CarService;
import com.portal.web.dto.CarDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by D064689 on 02.02.2017. such impl. wow.
 */

@Service
public class CarServiceImpl
        implements CarService {

    @Autowired
    CarRepository rep;

    @Autowired
    BrandRepository brandRepository;

    @Autowired
    SellerRepository sellerRepository;

    @Autowired
    LocationRepository locationRepository;


    @Override
    public ArrayList<Car> getAllCars() {
        Iterator<Car> iteratorToArray =rep.findAll().iterator();
        return Lists.newArrayList(iteratorToArray);

    }


    @Override
    public Car insertCar(CarDTO input) {
        Car car1 = new Car();
        Brand brand = new Brand();
        brand.setBrandName(input.getBrandName());
        brand.setModel(input.getBrandModel());
        brandRepository.save(brand);

        Location location = new Location();
        location.setLocationName(input.getLocationName());
        location.setState(input.getLocationState());
        location.setPostalCode(input.getLocationPostalCode().toString());
        locationRepository.save(location);

        Seller seller = new Seller();
        seller.setLocation(location);
        seller.setTelephoneNumber(input.getSellerTelephoneNumber());
        seller.setMobileNumber(input.getSellerMobileNumber());
        seller.setFirstName(input.getSellerFirstName());
        seller.setLastName(input.getSellerLastName());
        seller.setEmail(input.getSellerEmail());
        seller.setCompanyName(input.getSellerCompanyName());
        seller.setLocation(location);
        sellerRepository.save(seller);

        car1.setBrand(brand);
        car1.setLocation(location);
        car1.setSeller(seller);

        car1.setFirstRegistration(input.getFirstRegistration());
        car1.setCarType(input.getCarType());
        car1.setBadge(input.getBadge());
        car1.setColor(input.getColor());
        car1.setFuelType(input.getFuelType());
        car1.setGear(input.getGear());
        car1.setHorsepower(input.getHorsepower());
        car1.setPrice(input.getPrice());

        rep.save(car1);
        return car1;
    }

    @Override
    public void deleteCar(int id) {
        rep.delete(id);
    }

    @Override
    public Car showCarById(int id) {
        Car c = new Car();
        c.setId(rep.findOne(1).getId());
        c.setCarType(rep.findOne(1).getCarType());
        c.setFirstRegistration(rep.findOne(1).getFirstRegistration());
        c.setKm(rep.findOne(1).getKm());
        c.setPrice(rep.findOne(1).getPrice());
        c.setHorsepower(rep.findOne(1).getHorsepower());
        c.setFuelType(rep.findOne(1).getFuelType());
        c.setGear(rep.findOne(1).getGear());
        c.setColor(rep.findOne(1).getColor());
        c.setBadge(rep.findOne(1).getBadge());
        c.setLocation(rep.findOne(1).getLocation());
        c.setBrand(rep.findOne(1).getBrand());
        c.setSeller(rep.findOne(1).getSeller());

        return c;
    }

}
