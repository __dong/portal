package com.portal.service.impl;
import com.google.common.collect.Lists;
import com.portal.domain.Seller;
import com.portal.repository.SellerRepository;
import com.portal.service.SellerService;
import com.portal.web.dto.SellerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by D064689 on 02.02.2017.
 */
@Service
public class SellerServiceImpl
        implements SellerService{


    @Autowired
    SellerRepository rep;


    @Override
    public Seller insertSeller(SellerDTO input) {
        Seller seller1 = new Seller();

        seller1.setFirstName(input.getFirstName());
        seller1.setLastName(input.getLastName());
        seller1.setCompanyName(input.getCompanyName());
        seller1.setMobileNumber(input.getMobileNumber());
        seller1.setTelephoneNumber(input.getTelephoneNumber());
        seller1.setEmail(input.getEmail());

        rep.save(seller1);
        return seller1;

    }

    @Override
    public void deleteSeller(int id) {
        rep.delete(id);
    }

    @Override
    public Seller searchSellerByCompanyName (String companyName) {
        return rep.findSellerByCompanyName(companyName);
    }


    @Override
    public Seller showSellerById(int id){
        Seller s = new Seller();
        s.setId(rep.findOne(1).getId());
        s.setFirstName(rep.findOne(1).getFirstName());
        s.setLastName(rep.findOne(1).getFirstName());
        s.setCompanyName(rep.findOne(1).getCompanyName());
        s.setMobileNumber(rep.findOne(1).getMobileNumber());
        s.setTelephoneNumber(rep.findOne(1).getTelephoneNumber());
        s.setEmail(rep.findOne(1).getEmail());
        s.setLocation(rep.findOne(1).getLocation());
        s.setCars(rep.findOne(1).getCars());

        return s;
    }

    @Override
    public ArrayList<Seller> getAllSellers() {
        Iterator<Seller> iteratorToArray = rep.findAll().iterator();
        return Lists.newArrayList(iteratorToArray);
    }

}
