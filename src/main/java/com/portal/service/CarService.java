package com.portal.service;
import com.portal.domain.Car;
import com.portal.web.dto.CarDTO;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by tdhoang on 05.01.17.
 *
 * Edited by ChrisHans on 02.02.17.
 */
@Configuration
@Transactional
public interface CarService {
    /**
     *
     * @param input
     * @return
     */
    Car insertCar(CarDTO input);

    /**
     *
     * @param id
     */
    void deleteCar(int id);

    /**
     *
     * @return
     */
    ArrayList<Car> getAllCars();

    /**
     *
     * @param id
     * @return
     */
    Car showCarById(int id);
}
