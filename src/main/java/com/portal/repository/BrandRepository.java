package com.portal.repository;

import com.portal.domain.Brand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
@Transactional
public interface BrandRepository extends CrudRepository<Brand, Integer> {

    Brand findBrandByBrandName(String brandName);

    List<Brand> findByBrandName(String brandName);

    List<Brand> findByModel(String model);



}
