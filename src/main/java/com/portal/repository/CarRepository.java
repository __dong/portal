package com.portal.repository;

import com.portal.domain.Car;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
@Transactional
public interface CarRepository extends CrudRepository<Car, Integer> {

    List<Car> findByColor(String color);

    List<Car> findByFuelType(String fuelType);


}
