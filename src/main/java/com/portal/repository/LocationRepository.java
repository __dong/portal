package com.portal.repository;

import com.portal.domain.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
@Transactional
public interface LocationRepository extends CrudRepository<Location, Integer> {

    Location findLocationByLocationName(String locationName);

    List<Location> findByLocationName(String locationName);

    List<Location> findByPostalCode(Integer postalCode);

    List<Location> findByState(String state);
}
