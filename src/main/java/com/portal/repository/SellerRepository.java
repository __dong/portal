package com.portal.repository;

import com.portal.domain.Seller;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tdhoang on 05.01.17.
 */
@Repository
@Transactional
public interface SellerRepository extends CrudRepository<Seller, Integer> {
    /**
     *
     * @param companyName
     * @return
     */
    Seller findSellerByCompanyName(String companyName);

    /**
     *
     * @param companyName
     * @return
     */
    List<Seller> findByCompanyName(String companyName);

    /**
     *
     * @param firstName
     * @return
     */
    List<Seller> findByFirstName(String firstName);

    /**
     *
     * @param lastName
     * @return
     */
    List<Seller> findByLastName(String lastName);

    /**
     *
     * @param emailAddress
     * @return
     */
    Seller findByEmail(String emailAddress);



}
