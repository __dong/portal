# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

#### How to use ####
pack: mvn clean install

### http ###
http://localhost:8080/home

### pack & run ###
mvn clean spring-boot:run


### make package jar ###
mvn clean install package

### run packed jar ###
java -jar target/portal-0.0.1-SNAPSHOT.jar

#### activate insert to db - run only one time for db ###
1. go to PortalApplication.java (main class)
2. remove "//" for activate execute the inserts
    * scroll to line 85 for brands
    * scroll to line 86 for locations
3. add "//" for deactivate the inserts

